import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { PersonCreateComponent } from './person-create/person-create.component';
import { PersonDetailComponent } from './person-detail/person-detail.component';
import { PersonEditComponent } from './person-edit/person-edit.component';
import { PersonComponent } from './person/person.component';
import { PersonService } from './person.service';


const appRoutes: Routes = [

  {
    path: 'people',
    component: PersonComponent,
    data: { title: 'People List' }
  },
  {
    path: 'person-details/:id',
    component: PersonDetailComponent,
    data: { title: 'Person Details' }
  },
  {
    path: 'person-create',
    component: PersonCreateComponent,
    data: { title: 'Create Person' }
  },
  {
    path: 'person-edit/:id',
    component: PersonEditComponent,
    data: { title: 'Edit Person' }
  },
  { path: '',
    redirectTo: '/people',
    pathMatch: 'full'
  }

];


@NgModule({
  declarations: [
    AppComponent,
    PersonCreateComponent,
    PersonDetailComponent,
    PersonEditComponent,
    PersonComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    PersonService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
