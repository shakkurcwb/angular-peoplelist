import { Component, OnInit } from '@angular/core';


import { Person } from './../person';


@Component({
  selector: 'app-person-create',
  templateUrl: './person-create.component.html',
  styleUrls: ['./person-create.component.css']
})
export class PersonCreateComponent implements OnInit {

  person: Person = new Person();

  constructor() {
  }

  ngOnInit() {
  }

  save(): void {
     alert('Saved.');
  }

}
