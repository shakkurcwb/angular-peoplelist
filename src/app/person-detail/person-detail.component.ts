import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


import { Person } from './../person';
import { PersonService } from './../person.service';


@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.css']
})
export class PersonDetailComponent implements OnInit {

  person: Person = new Person();

  constructor(private router: Router,
              private route: ActivatedRoute,
              private personService: PersonService) { }

  ngOnInit() {
    this.get(this.route.snapshot.params['id']);
  }

  get(id): void {
    this.personService.getId(id)
      .subscribe(person => this.person = person);
  }

  delete(): void {
    if (confirm('Are you sure to delete ' + this.person.name + '?')) {
      alert('Deleted.');
    }
  }

}
