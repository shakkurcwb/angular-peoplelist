import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


import { Person } from './../person';
import { PersonService } from './../person.service';


@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.css']
})
export class PersonEditComponent implements OnInit {

  person: Person = new Person();

  constructor(private router: Router,
              private route: ActivatedRoute,
              private personService: PersonService) { }

  ngOnInit() {
    this.get(this.route.snapshot.params['id']);
  }

  get(id): void {
    this.personService.getId(id)
      .subscribe(person => this.person = person);
  }

  update(): void {
    alert('Updated.');
  }

}
