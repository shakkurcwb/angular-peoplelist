export const People: any = [
  {
    id: 1,
    name: 'MARLON FERREIRA',
    age: 25,
    country: 'CANADA',
    score: 766
  },
  {
    id: 2,
    name: 'MAYRON CECCON',
    age: 27,
    country: 'CHINA',
    score: 841
  },
  {
    id: 3,
    name: 'ALEXANDRE MARCOLINO',
    age: 28,
    country: 'SWEDEN',
    score: 658
  },
  {
    id: 4,
    name: 'FERNANDO DINIZ',
    age: 44,
    country: 'ARGENTINA',
    score: 158
  }
];