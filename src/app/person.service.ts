import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';


import { Person } from './person';
import { People } from './person.mock';


@Injectable()
export class PersonService {

  constructor() { }

  get(): Observable<Person> {
    return of (People);
  }

  getId(k): Observable<Person> {
    return of (People.find(function (d) {
      if (k == d.id) return d;
    }));
  }

}
