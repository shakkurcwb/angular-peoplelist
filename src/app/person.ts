export class Person {

    public id: Number = null;
    public name: String = null;
    public age: Number = null;
    public country: String = null;
    public score: Number = null;
    
}
